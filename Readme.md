# Mazegen

WASM module for generating mazes.

## Attribution

DFS, Kruskal's Algorithm, Wilson's Algorithm, Eller's Algorithm, Prim's Algorithm are all
based on [implementation](https://github.com/CianLR/mazegen-rs) provided by
[CianLR](https://github.com/CianLR) and modified to allow mazes with different dimensions
to be used. His code is being provided under MIT License as is this WASM module.

## License

This module is provided under the MIT License.
A copy of the license is available in the [License.md](License.md) file.
