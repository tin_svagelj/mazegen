use std::io::*;

use rand::prelude::*;

use crate::algo::MazeGenerator;
use crate::maze::*;

pub struct DFSAlgo<T: RngCore> {
    rand: T
}

impl<T: RngCore> DFSAlgo<T> {
    pub fn new(rand: T) -> DFSAlgo<T> {
        DFSAlgo { rand }
    }

    fn dfs(&mut self, maze: &mut Maze, visited: &mut Vec<Vec<bool>>, pos: CellPos) -> Result<()> {
        visited[pos.x as usize][pos.y as usize] = true;

        let mut adj: Vec<CellPos> = maze.adjacent(pos);
        adj.shuffle(&mut self.rand);

        while !adj.is_empty() {
            let other = adj.pop().expect("adj can't be empty");
            if visited[other.x as usize][other.y as usize] {
                continue;
            }
            maze.connect(pos, other);
            self.dfs(maze, visited, other)?;
        }

        Ok(())
    }
}

impl<T: RngCore> MazeGenerator for DFSAlgo<T> {
    fn generate(&mut self, size: MazeSize) -> Result<Maze> {
        let mut maze = Maze::new(size);
        let mut visited = vec![vec![false; size.y as usize]; size.x as usize];

        self.dfs(&mut maze, &mut visited, CellPos { x: size.x / 2 , y: size.y / 2 })?;

        Ok(maze)
    }
}


#[cfg(test)]
mod test {
    use crate::algo::MazeAlgorithm;
    use crate::algo::test_util::*;

    #[test]
    fn test_is_perfect() {
        let m = apply_test_algo(MazeAlgorithm::DFS);
        assert!(is_perfect_maze(&m));
    }
}
