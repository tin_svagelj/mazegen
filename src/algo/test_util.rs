use rand::SeedableRng;

use crate::algo::{generate_maze, MazeAlgorithm};
use crate::maze::{CellPos, CellWall, Maze, MazeSize};

#[allow(dead_code)]
pub fn apply_test_algo(algo: MazeAlgorithm) -> Maze {
    // Persistent seed is useful for debugging.
    let seed: [u8; 16] = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    let rng = rand_pcg::Pcg32::from_seed(seed);

    generate_maze(algo, rng, MazeSize { x: 25, y: 4 })
        .expect("Maze should've generated successfully.")
}

#[allow(dead_code)]
pub fn is_perfect_maze(maze: &Maze) -> bool {
    let size = maze.get_size();
    let mut seen = vec![vec![false; size.y as usize]; size.x as usize];
    let mut stack = vec![(0, 0, 0, 0)];
    seen[0][0] = true;
    while !stack.is_empty() {
        let (x, y, px, py) = stack.pop().unwrap();
        for p2 in maze.adjacent_open(CellPos { x, y }) {
            if !seen[p2.x as usize][p2.y as usize] {
                seen[p2.x as usize][p2.y as usize] = true;
                stack.push((p2.x, p2.y, x, y));
            } else if p2.x != px || p2.y != py {
                // If the seen cell is not the parent then it's a loop.
                return false;
            }
        }
    }

    // Return true if everything in seen is true.
    seen.into_iter().flatten().all(|it| it)
}

#[allow(dead_code)]
pub fn print_maze(maze: &Maze) {
    // Top row
    let mut top = "┏━".to_string();
    for x in 0..(maze.get_size().x - 1) {
        top.push_str(if maze.get_wall(CellPos { x, y: 0 }, CellWall::Right) { "┳" } else { "━" });
        top.push_str("━");
    }
    top.push('┓');
    println!("{}", top);
    // Middle rows
    for y in 0..(maze.get_size().y - 1) {
        // Horizontal border
        let mut horz = if maze.get_wall(CellPos { x: 0, y }, CellWall::Bottom) {
            "┣━".to_string()
        } else {
            "┃ ".to_string()
        };
        for x in 1..maze.get_size().x {
            horz.push(
                get_inner_junction(
                    maze,
                    CellPos { x: x - 1, y },
                    CellPos { x, y: y + 1 },
                ));
            horz.push_str(
                if maze.get_wall(CellPos { x, y }, CellWall::Bottom) { "━" } else { " " });
        }
        horz.push(
            if maze.get_wall(CellPos { x: maze.get_size().x - 1, y }, CellWall::Bottom) { '┫' } else { '┃' });
        println!("{}", horz);
    }
    // Final line
    let mut bot = "┗━".to_string();
    for x in 0..maze.get_size().x - 1 {
        bot.push_str(
            if maze.get_wall(CellPos { x, y: maze.get_size().y - 1 }, CellWall::Right) {
                "┻"
            } else {
                "━"
            });
        bot.push_str("━");
    }
    bot.push('┛');
    println!("{}", bot);
}

fn get_inner_junction(maze: &Maze, a: CellPos, d: CellPos) -> char {
    // The inner junction is the piece connecting four squares of a maze.
    // a b
    //  ?
    // c d
    //
    // The ? can be one of ┳, ┃, ╋, ... depending on the walls between
    // each of the cells. Because every cell stores all of its walls we
    // can determine which junction to use based on the down and right
    // of 'a' and the up and left of 'd' in the diagram
    //
    // Because the cells are bit vectors and there's no overlap of the
    // bits needed from 'a' or 'd' we can combine them into a single
    // i8 and use that as a lookup. Note this will break if the values
    // in the enum change. (Is this bad practice? Feels like it).
    let mut lookup: i8 = 0;
    lookup |= if maze.get_wall(d, CellWall::Top) { 1 << 2 } else { 0 };
    lookup |= if maze.get_wall(a, CellWall::Right) { 1 << 1 } else { 0 };
    lookup |= if maze.get_wall(a, CellWall::Bottom) { 1 << 3 } else { 0 };
    lookup |= if maze.get_wall(d, CellWall::Left) { 1 << 0 } else { 0 };
    [' ', '╻', '╹', '┃', '╺', '┏', '┗', '┣',
        '╸', '┓', '┛', '┫', '━', '┳', '┻', '╋'][lookup as usize]
}
