use std::io::*;

use rand::prelude::*;

use crate::algo::MazeGenerator;
use crate::maze::*;

pub struct PrimAlgo<T: RngCore> {
    rand: T
}

impl<T: RngCore> PrimAlgo<T> {
    pub fn new(rand: T) -> PrimAlgo<T> {
        PrimAlgo { rand }
    }

    fn prim(&mut self, maze: &mut Maze) -> Result<()> {
        let size = maze.get_size();
        let mut in_maze = vec![vec![false; size.y as usize]; size.x as usize];
        let mut walls: Vec<_> = maze.adjacent(CellPos { x: 0 , y: 0 })
            .into_iter()
            .map(|p| (0, 0, p.x, p.y))
            .collect();
        in_maze[0][0] = true;
        while !walls.is_empty() {
            let (x, y, x2, y2) = walls.remove(self.rand.gen_range(0, walls.len()));
            if in_maze[x2 as usize][y2 as usize] {
                continue;  // Already connected.
            }
            in_maze[x2 as usize][y2 as usize] = true;
            maze.connect(CellPos { x, y }, CellPos { x: x2 , y: y2 });
            walls.extend(maze.adjacent(CellPos { x: x2 , y: y2 })
                .into_iter()
                .filter(|&p| !in_maze[p.x as usize][p.y as usize])
                .map(|CellPos { x: x3 , y: y3 }| (x2, y2, x3, y3)));
        }
        Ok(())
    }
}

impl<T: RngCore> MazeGenerator for PrimAlgo<T> {
    fn generate(&mut self, size: MazeSize) -> Result<Maze> {
        let mut maze = Maze::new(size);

        self.prim(&mut maze)?;

        Ok(maze)
    }
}


#[cfg(test)]
mod test {
    use crate::algo::MazeAlgorithm;
    use crate::algo::test_util::*;

    #[test]
    fn test_is_perfect() {
        let m = apply_test_algo(MazeAlgorithm::Prim);
        assert!(is_perfect_maze(&m));
    }
}
