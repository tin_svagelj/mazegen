use rand::prelude::*;

use crate::algo::*;
use crate::algo::MazeGenerator;
use crate::maze::*;

use std::io::*;

pub struct KruskalAlgo<T: RngCore> {
    rand: T
}

impl<T: RngCore> KruskalAlgo<T> {
    pub fn new(rand: T) -> KruskalAlgo<T> {
        KruskalAlgo { rand }
    }

    fn kruskal(&mut self, maze: &mut Maze) -> Result<()> {
        let mut uf = util::UnionFind::new();
        let mut walls = vec![];
        let size = maze.get_size();
        for x in 0..size.x {
            for y in 0..size.y {
                for p2 in maze.adjacent(CellPos { x, y }) {
                    walls.push((x, y, p2.x, p2.y));
                }
            }
        }
        walls.shuffle(&mut self.rand);
        while !walls.is_empty() {
            let (x, y, x2, y2) = walls.pop().unwrap();
            if uf.connected(&CellPos { x, y }, &CellPos { x: x2 , y: y2 }) {
                continue;
            }
            uf.join(&CellPos { x, y }, &CellPos { x: x2 , y: y2 });
            maze.connect(CellPos { x, y }, CellPos { x: x2 , y: y2 });
        }
        Ok(())
    }
}

impl<T: RngCore> MazeGenerator for KruskalAlgo<T> {
    fn generate(&mut self, size: MazeSize) -> Result<Maze> {
        let mut maze = Maze::new(size);

        self.kruskal(&mut maze)?;

        Ok(maze)
    }
}


#[cfg(test)]
mod test {
    use crate::algo::test_util::*;
    use crate::algo::MazeAlgorithm;

    #[test]
    fn test_is_perfect() {
        let m = apply_test_algo(MazeAlgorithm::Kruskal);
        assert!(is_perfect_maze(&m));
    }
}
