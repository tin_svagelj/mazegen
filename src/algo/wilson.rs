use rand::prelude::*;

use crate::algo::MazeGenerator;
use crate::maze::*;
use std::io::*;

#[derive(Clone, PartialEq)]
enum CellInfo {
    Blank,
    CurPath,
    InMaze,
}

pub struct WilsonAlgo<T: RngCore> {
    rand: T
}

impl<T: RngCore> WilsonAlgo<T> {
    pub fn new(rand: T) -> WilsonAlgo<T> {
        WilsonAlgo { rand }
    }

    fn wilson(&mut self, mut maze: &mut Maze) -> Result<()> {
        let size = maze.get_size();
        let mut cells = vec![vec![CellInfo::Blank; size.y as usize]; size.x as usize];
        cells[0][0] = CellInfo::InMaze;
        for x in 0..size.x - 1 {
            for y in 0..size.y {
                if x == 1 && y == 1 {
                    continue;
                }
                let a = self.walk(&mut maze, &mut cells, x, y, x, y);
                if a.is_some() {
                    return Err(Error::new(ErrorKind::InvalidData, "wilson paths exhausted"));
                }
            }
        }
        Ok(())
    }

    fn walk(&mut self, mut maze: &mut Maze, mut cells: &mut Vec<Vec<CellInfo>>, x: MazeDim, y: MazeDim, px: MazeDim, py: MazeDim) -> Option<CellPos> {
        if cells[x as usize][y as usize] == CellInfo::InMaze {
            return None;
        } else if cells[x as usize][y as usize] == CellInfo::CurPath {
            return Some(CellPos { x, y });
        }
        cells[x as usize][y as usize] = CellInfo::CurPath;
        let mut adj = maze.adjacent(CellPos { x, y });
        adj.shuffle(&mut self.rand);
        while !adj.is_empty() {
            let p2 = adj.pop().unwrap();
            if p2.x == px && p2.y == py {
                continue;  //  Don't move backwards.
            }
            let res = self.walk(&mut maze, &mut cells, p2.x, p2.y, x, y);
            if res.is_none() {
                cells[x as usize][y as usize] = CellInfo::InMaze;
                maze.connect(CellPos { x, y }, p2);
                return None;
            }
            // We're erasing the loop.
            let l = res.unwrap();
            if l.x != x || l.y != y {
                cells[x as usize][y as usize] = CellInfo::Blank;
                return Some(l);
            }
        }
        // All paths exhausted.
        cells[x as usize][y as usize] = CellInfo::Blank;
        Some(CellPos { x: px , y: py })
    }
}

impl<T: RngCore> MazeGenerator for WilsonAlgo<T> {
    fn generate(&mut self, size: MazeSize) -> Result<Maze> {
        let mut maze = Maze::new(size);

        self.wilson(&mut maze)?;

        Ok(maze)
    }
}


#[cfg(test)]
mod test {
    use crate::algo::test_util::*;
    use crate::algo::MazeAlgorithm;

    #[test]
    fn test_is_perfect() {
        let m = apply_test_algo(MazeAlgorithm::Wilson);
        assert!(is_perfect_maze(&m));
    }
}
