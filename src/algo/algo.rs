use std::io::*;

use rand::RngCore;

use crate::algo::*;
use crate::maze::*;

pub enum MazeAlgorithm {
    DFS,
    Eller,
    Kruskal,
    Prim,
    Wilson,
}

impl MazeAlgorithm {
    pub fn parse(name: &String) -> Option<MazeAlgorithm> {
        match name.to_lowercase().as_ref() {
            "dfs" => Some(MazeAlgorithm::DFS),
            "eller" => Some(MazeAlgorithm::Eller),
            "kruskal" => Some(MazeAlgorithm::Kruskal),
            "prim" => Some(MazeAlgorithm::Prim),
            "wilson" => Some(MazeAlgorithm::Wilson),
            _ => None,
        }
    }
}

pub trait MazeGenerator {
    fn generate(&mut self, size: MazeSize) -> Result<Maze>;
}

pub fn generate_maze<T: RngCore>(algorithm: MazeAlgorithm, rand: T, size: MazeSize) -> Result<Maze> {
    match algorithm {
        MazeAlgorithm::DFS => DFSAlgo::new(rand).generate(size),
        MazeAlgorithm::Eller => EllerAlgo::new(rand).generate(size),
        MazeAlgorithm::Kruskal => KruskalAlgo::new(rand).generate(size),
        MazeAlgorithm::Prim => PrimAlgo::new(rand).generate(size),
        MazeAlgorithm::Wilson => WilsonAlgo::new(rand).generate(size),
    }
}
