use std::collections::HashSet;
use std::io::*;
use std::ops::Range;

use rand::prelude::*;

use crate::algo::MazeGenerator;
use crate::maze::*;

/// Eller's Algorithm State
///
/// # Stored Data
///
/// * `rand` - Randomizer used to generate the maze
/// * `wall_bias` - Bias towards vertical/horizontal walls.
///                 Must be in range `0.0 > wall_bias > 1.0`:
///     * 0.0 - all walls are vertical
///     * 1.0 - all walls are horizontal
pub struct EllerAlgo<T: RngCore> {
    rand: T,
    wall_bias: f64,
}

impl<T: RngCore> EllerAlgo<T> {
    pub fn new(rand: T) -> EllerAlgo<T> {
        EllerAlgo {
            rand,
            wall_bias: 0.5,
        }
    }

    fn eller(&mut self, mut maze: &mut Maze) {
        let size = maze.get_size();

        let mut curr = (0..size.x).collect();
        let mut last_dropped = HashSet::new();
        for row in 0..size.y {
            last_dropped = self.generate_row(&mut maze, &mut curr, last_dropped, row);
        }
    }

    fn generate_row(
        &mut self,
        maze: &mut Maze,
        row_data: &mut Vec<u32>,
        dropped_sets: HashSet<MazeDim>,
        row: MazeDim,
    ) -> HashSet<MazeDim> {
        let maze_size = maze.get_size();

        if row != 0 {
            // If not on first row, prepare this row by leaving vertically opened cells the same
            // and changing sets of others to unique values
            let mut curr = CellPos { x: 0, y: row };
            let mut i = 0;
            for col in 0..maze_size.x {
                curr.x = col;
                if maze.get_wall(curr, CellWall::Top) {
                    while dropped_sets.contains(&i) {
                        i += 1;
                    }
                    row_data[col as usize] = i;
                }
            }
        }
        
        if row != maze_size.y - 1 {
            let mut sets = vec![];
            let mut set_start = 0;

            for col in 1..maze_size.x as usize {
                let prev = row_data[col - 1];
                let curr = row_data[col];
                if prev != curr {
                    if self.rand.gen_bool(self.wall_bias) {
                        // Append this cell to the last set and cell
                        row_data[col] = prev;
                        maze.connect(
                            CellPos {
                                x: (col - 1) as u32,
                                y: row,
                            },
                            CellPos {
                                x: col as u32,
                                y: row,
                            },
                        );
                    } else {
                        // Create a new set
                        sets.push(set_start..col);
                        set_start = col;
                    }
                } // else must leave wall
            }
            sets.push(set_start..maze_size.x as usize);

            for set in &sets {
                self.drop_random(maze, set.clone(), row);
            }

            sets.into_iter().map(|it| row_data[it.start]).collect()
        } else {
            // We do the same thing for last row but without dropping down or possibility of
            // isolated sets
            for col in 0..(maze_size.x - 1) as usize {
                if row_data[col] != row_data[col + 1] {
                    maze.connect(
                        CellPos {
                            x: col as u32,
                            y: row,
                        },
                        CellPos {
                            x: (col + 1) as u32,
                            y: row,
                        },
                    );

                    fill_set(row_data, col, row_data[col + 1])
                } // else must leave wall
            }
            HashSet::new()
        }
    }

    fn drop_random(&mut self, maze: &mut Maze, col_range: Range<usize>, row: MazeDim) {
        let mut dropping: Vec<usize> = col_range.clone().collect();
        dropping.shuffle(&mut self.rand);

        let n_down = if col_range.len() == 1 {
            1
        } else {
            self.rand.gen_range(1, col_range.len())
        };
        dropping.truncate(n_down);

        // Open random n indices from col_range down
        for d in dropping {
            maze.connect(
                CellPos {
                    x: d as MazeDim,
                    y: row,
                },
                CellPos {
                    x: d as MazeDim,
                    y: row + 1,
                },
            )
        }
    }
}

fn fill_set(row_data: &mut Vec<u32>, which: usize, with: u32) {
    let same_as = row_data[which as usize];
    // from could be const
    let mut from = which;
    while from > 0 {
        if row_data[from] != same_as {
            break;
        }
        from -= 1;
    }
    // to could be const
    let mut to = which;
    while to < row_data.len() {
        if row_data[to] != same_as {
            break;
        }
        to += 1;
    }

    for i in from..to {
        row_data[i] = with;
    }
}

impl<T: RngCore> MazeGenerator for EllerAlgo<T> {
    fn generate(&mut self, size: MazeSize) -> Result<Maze> {
        let mut maze = Maze::new(size);

        self.eller(&mut maze);

        Ok(maze)
    }
}

#[cfg(test)]
mod test {
    use crate::algo::test_util::*;
    use crate::algo::MazeAlgorithm;

    #[test]
    fn test_is_perfect() {
        let m = apply_test_algo(MazeAlgorithm::Eller);
        if !is_perfect_maze(&m) {
            print_maze(&m);
            panic!("Not a perfect maze!");
        }
    }
}
