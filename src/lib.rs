// #[macro_use]
// extern crate serde_derive;

use rand::SeedableRng;
use wasm_bindgen::prelude::*;

use crate::algo::MazeAlgorithm;
use crate::maze::CellPos;

pub mod algo;
pub mod maze;

#[macro_export]
macro_rules! console_error {
    ($($t:tt)*) => {{
        #[cfg(feature="std")]
        web_sys::console::error_1(&( &format_args!($($t)*).to_string().into()) )
    }};
}

#[wasm_bindgen]
pub fn generate_maze(width: u32, height: u32, algorithm: &JsValue, seed: &JsValue) -> JsValue {
    let algoritm = if algorithm.is_string() {
        let alg_str = algorithm.as_string().unwrap_or(String::new());
        match MazeAlgorithm::parse(&alg_str) {
            Some(it) => it,
            None => {
                console_error!("Received invalid algorithm name: ", alg_str);
                return JsValue::null();
            }
        }
    } else {
        console_error!("Algorithm name must be a string.");
        return JsValue::null();
    };

    let seed = if seed.is_string() {
        let str_val = seed.as_string().unwrap();
        let mut res = [0u8; 16];
        str_val.chars()
            .enumerate()
            .for_each(|it| res[it.0 % 16] += it.1 as u8);
        res
    } else {
        rand::random()
    };

    let rng = rand_pcg::Pcg32::from_seed(seed);

    let maze = algo::generate_maze(algoritm, rng, CellPos { x: width , y: height });

    match maze {
        Ok(maze) => {
            JsValue::from(maze)
        },
        Err(_) => {
            console_error!("Internal maze generator error.");
            JsValue::null()
        }
    }
}
