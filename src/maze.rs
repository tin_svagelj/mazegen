use bit_vec::BitVec;
use wasm_bindgen::__rt::core::cmp::Ordering;
use wasm_bindgen::prelude::*;

pub type MazeDim = u32;

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, PartialOrd, PartialEq, Eq, Hash)]
pub struct CellPos {
    pub x: MazeDim,
    pub y: MazeDim,
}

impl CellPos {
    pub fn relative(&self, posX: i64, posY: i64) -> CellPos {
        CellPos {
            x: (self.x as i64 + posX) as MazeDim,
            y: (self.y as i64 + posY) as MazeDim,
        }
    }
}

impl std::cmp::Ord for CellPos {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.x > other.x {
            Ordering::Greater
        } else if other.x > self.x {
            Ordering::Less
        } else {
            if self.y > other.y {
                Ordering::Greater
            } else if other.y > self.y {
                Ordering::Less
            } else {
                Ordering::Equal
            }
        }
    }
}

pub type MazeSize = CellPos;

#[wasm_bindgen]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum CellWall {
    Top = 1,
    Right = 2,
    Bottom = 3,
    Left = 4,
}

impl CellWall {
    pub fn adjacent(&self, pos: CellPos) -> CellPos {
        match self {
            CellWall::Top => CellPos { x: pos.x, y: pos.y + 1 },
            CellWall::Right => CellPos { x: pos.x + 1, y: pos.y },
            CellWall::Bottom => CellPos { x: pos.x, y: pos.y - 1 },
            CellWall::Left => CellPos { x: pos.x - 1, y: pos.y },
        }
    }
}

#[wasm_bindgen]
pub struct Maze {
    size: MazeSize,
    walls: BitVec,
}

impl Maze {
    pub fn new(size: MazeSize) -> Maze {
        let walls = BitVec::from_elem(
            ((size.x - 1) * size.y) as usize + // vertical
                ((size.y - 1) * size.x) as usize // horizontal
            , true);
        Maze { size, walls }
    }

    pub fn get_size(&self) -> MazeSize {
        self.size
    }

    fn wall_index(&self, pos: CellPos, wall: CellWall) -> usize {
        (pos.x as i64 + if wall != CellWall::Right {
            0
        } else {
            1
        } + if wall == CellWall::Left || wall == CellWall::Right {
            (self.size.x as i64 * 2 - 1) * pos.y as i64 - 1
        } else {
            let vert = pos.y as i64 + if wall == CellWall::Top {
                0
            } else {
                1
            };
            let horiz = pos.y as i64 - if wall == CellWall::Top {
                1
            } else {
                0
            };
            (self.size.x as i64 - 1) * vert + self.size.x as i64 * horiz
        }) as usize
    }

    fn get_wall_unchecked(&self, pos: CellPos, wall: CellWall) -> bool {
        pos.x == 0 && wall == CellWall::Left ||
            pos.y == 0 && wall == CellWall::Top ||
            pos.x == self.size.x - 1 && wall == CellWall::Right ||
            pos.y == self.size.y - 1 && wall == CellWall::Bottom ||
            self.walls[self.wall_index(pos, wall)]
    }

    pub fn get_wall(&self, pos: CellPos, wall: CellWall) -> bool {
        if pos.x >= self.size.x || pos.y >= self.size.y {
            false
        } else {
            self.get_wall_unchecked(pos, wall)
        }
    }

    pub fn get_walls(&self, pos: CellPos) -> Vec<CellWall> {
        if pos.x >= self.size.x || pos.y >= self.size.y {
            vec![]
        } else {
            vec![
                CellWall::Top,
                CellWall::Right,
                CellWall::Bottom,
                CellWall::Left
            ].into_iter()
                .map(|it| if self.get_wall_unchecked(pos, it) { Some(it) } else { None })
                .flatten()
                .collect()
        }
    }

    pub fn set_wall(&mut self, pos: CellPos, wall: CellWall, state: bool) {
        if pos.x >= self.size.x || pos.y >= self.size.y {
            return;
        } else {
            self.walls.set(self.wall_index(pos, wall), state)
        }
    }

    pub fn adjacent(&self, pos: CellPos) -> Vec<CellPos> {
        vec![
            if pos.x > 0 { Some(CellPos { x: pos.x - 1, y: pos.y }) } else { None },
            if pos.y > 0 { Some(CellPos { x: pos.x, y: pos.y - 1 }) } else { None },
            if pos.x < self.size.x - 1 { Some(CellPos { x: pos.x + 1, y: pos.y }) } else { None },
            if pos.y < self.size.y - 1 { Some(CellPos { x: pos.x, y: pos.y + 1 }) } else { None }
        ].into_iter().flatten().collect()
    }

    pub fn adjacent_open(&self, pos: CellPos) -> Vec<CellPos> {
        self.adjacent(pos)
            .into_iter()
            .filter(|&other| !self.get_wall_between(pos, other))
            .collect()
    }

    pub fn get_wall_between(&self, a: CellPos, b: CellPos) -> bool {
        let min = std::cmp::min(a, b);
        let max = std::cmp::max(a, b);
        let wall = if min.x < max.x { CellWall::Right } else { CellWall::Bottom };
        self.walls[self.wall_index(min, wall)]
    }

    pub fn connect(&mut self, a: CellPos, b: CellPos) {
        let min = std::cmp::min(a, b);
        let max = std::cmp::max(a, b);
        let wall = if min.x < max.x { CellWall::Right } else { CellWall::Bottom };
        self.walls.set(self.wall_index(min, wall), false)
    }
}
